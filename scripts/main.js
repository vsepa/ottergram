const DETAIL_IMAGE_SELECTOR = '[data-image-role="target"]';
const DETAIL_TITLE_SELECTOR = '[data-image-role="title"]';
const THUMBNAIL_LINK_SELECTOR = '[data-image-role="trigger"]';

const setDetails = (imageUrl, titleText) => {
  let detailImage = document.querySelector(DETAIL_IMAGE_SELECTOR);
  detailImage.setAttribute('src', imageUrl);

  var detailTitle = document.querySelector(DETAIL_TITLE_SELECTOR);
  detailTitle.textContent = titleText;
};

const imageFromThumb = thumbnail => thumbnail.getAttribute('data-image-url');

const titleFromThumb = thumbnail => thumbnail.getAttribute('data-image-title');

const setDetailsFromThumb = thumbnail => {
  setDetails(imageFromThumb(thumbnail), titleFromThumb(thumbnail));
};

const addThumbClickHandler = thumb => {
  thumb.addEventListener('click', (event) => {
    event.preventDefault();
    setDetailsFromThumb(thumb);
  });
};

const getThumbnailsArray = () => {
  let thumbnails = document.querySelectorAll(THUMBNAIL_LINK_SELECTOR);
  let thumbnailArray = [].slice.call(thumbnails);
  return thumbnailArray;
};

const initializeEvents = () => {
  let thumbnails = getThumbnailsArray();
  thumbnails.forEach(addThumbClickHandler);
};

initializeEvents();
